class Application {
    constructor(agentCount, height, width, cellSize, agentRadius) {
        this.agents = [];
        this.serverMessageCount = 0;
        this.socket = new WebSocket("ws://localhost:8080/ws");
        this.cellSize = cellSize;
        this.agentCount = agentCount;
        this.agentRadius = agentRadius;
        this.zoomScaleFactor = 1;
        this.zoomScaleMultiplier = 1.1;
        this.dragging = false;
        this.height = height;
        this.width = width;
        this.selectedAgent = 0;
        this.mousePos = {x: 0, y: 0};
    }

    initialize() {
        this.app = this.setUpPixiApp(window.innerWidth, window.innerHeight);
        this.agentTexture = this.generateAgentTexture(this.agentRadius);
        this.particleContainer = this.setUpContainer();
        this.grid = this.setUpGrid();
        this.socket.addEventListener("message", (event) => this.handleWebSocketMessage(event));
        this.setUpPeriodicUpdates();
        this.addMouseInteractions();
    }

    setUpPixiApp(width, height) {
        const app = new PIXI.Application({ width, height, backgroundColor: 0x777777 });
        document.getElementById('canvas-container').appendChild(app.view);
        return app;
    }

    generateAgentTexture(agentSize) {
        let graphics = new PIXI.Graphics();
        graphics.beginFill(0xFFFFFF);
        graphics.drawCircle(0, 0, agentSize);
        graphics.endFill();
        return this.app.renderer.generateTexture(graphics);
    }

    setUpContainer() {
        let container = new PIXI.Container();
        this.app.stage.addChild(container);
        return container;
    }

    setUpGrid() {
        let grid = new PIXI.Graphics();
        grid.lineStyle(1, 0x000000, 0.1);
        for (let i = 0; i <= this.width; i += this.cellSize) {
            grid.moveTo(i, 0);
            grid.lineTo(i, this.height);
        }
        for (let i = 0; i <= this.height; i += this.cellSize) {
            grid.moveTo(0, i);
            grid.lineTo(this.width, i);
        }
        this.app.stage.addChild(grid);
        return grid;
    }

    handleWebSocketMessage(event) {
        this.serverMessageCount++;
        const data = JSON.parse(event.data);
        this.updateAgents(data);
    }

    updateAgents(newAgents) {
        this.updateDebugInfo();
        newAgents.forEach((newAgent, i) => {
            let agent = this.agents[i] || new PIXI.Sprite(this.agentTexture);
            if (!this.agents[i]) {
                this.particleContainer.addChild(agent);
                this.agents[i] = agent;
            }
            agent.tint = newAgent.color === 'Red' ? 0xFF0000 : 0x00FF00;
            agent.position.set(newAgent.pos[0], newAgent.pos[1]);
            if (this.selectedAgent == i) {
                agent.alpha = 1
            } else {
                agent.alpha = 0.3
            }
        });
        document.getElementById("agentcount").innerHTML = this.agents.length.toString();
    }

    setUpPeriodicUpdates() {
        setInterval(() => {
            document.getElementById("framerate").innerHTML = `FPS: ${this.app.ticker.FPS.toFixed(2)}`;
        }, 100);

        setInterval(() => {
            document.getElementById("tickrate").innerHTML = `Tick rate: ${this.serverMessageCount}`;
            this.serverMessageCount = 0;
        }, 1000);
    }

    addMouseInteractions() {
        this.app.view.addEventListener('wheel', (e) => {
            e.preventDefault();
        
            const oldZoomScaleFactor = this.zoomScaleFactor;
            this.zoomScaleFactor = e.deltaY < 0 ? 
                this.zoomScaleFactor * this.zoomScaleMultiplier : 
                this.zoomScaleFactor / this.zoomScaleMultiplier;
            
            const centerX = this.app.renderer.width / 2;
            const centerY = this.app.renderer.height / 2;

            // on calcule la nouvelle position de l'écran après le zoom
            const newPosX = (this.particleContainer.x - centerX) * (this.zoomScaleFactor / oldZoomScaleFactor) + centerX;
            const newPosY = (this.particleContainer.y - centerY) * (this.zoomScaleFactor / oldZoomScaleFactor) + centerY;
        
            this.particleContainer.scale.set(this.zoomScaleFactor);
            this.particleContainer.position.set(newPosX, newPosY);

            this.grid.scale.set(this.zoomScaleFactor);
            this.grid.position.set(newPosX, newPosY);
        });

        this.app.view.addEventListener('mousedown', (e) => { 
            this.mousePos.x = e.clientX;
            this.mousePos.y = e.clientY;
            if (e.button === 0) {
                const mousePos = this.mousePos;
                mousePos.x -= this.particleContainer.x;
                mousePos.y -= this.particleContainer.y;
                let closestDistance = Infinity;
                let closestAgentIndex = null;
    
                this.agents.forEach((agent, index) => {
                    const dx = agent.x * this.zoomScaleFactor - mousePos.x;
                    const dy = agent.y * this.zoomScaleFactor - mousePos.y;
                    const distanceSquared = Math.sqrt(dx * dx + dy * dy); // Should use sqrt but we don't care about the real distance, we want only the closest
    
                    if (distanceSquared < closestDistance) {
                        closestDistance = distanceSquared;
                        closestAgentIndex = index;
                    }
                });
    
                if (closestAgentIndex !== null) {
                    fetch(`http://localhost:8080/selectAgent`, 
                        { "method": "POST", "body": '{ "agentID": ' + closestAgentIndex.toString() + "}" }, 
                        { "Content-Type": "application/json" }
                    )
                    this.highlightAgent(closestAgentIndex);
                    this.updateAgentInfo(closestAgentIndex);
                }
            }

            this.dragging = true;

            

        });
        this.app.view.addEventListener('mouseup', () => { this.dragging = false; });
        this.app.view.addEventListener('mousemove', (e) => {
            this.mousePos.x = e.clientX;
            this.mousePos.y = e.clientY;
            if (this.dragging) {
                this.particleContainer.x += e.movementX;
                this.particleContainer.y += e.movementY;
                this.grid.x += e.movementX;
                this.grid.y += e.movementY;
            }
        });
    }

    highlightAgent(agentIndex) {
        // Reset previous selection if needed
        if (this.selectedAgent !== null) {
            this.resetAgentAppearance(this.selectedAgent);
        }
        // Update appearance to highlight the agent
        const agent = this.agents[agentIndex];
        agent.alpha = 1; // or any other effect
        this.selectedAgent = agentIndex;
    }
    
    resetAgentAppearance(agentIndex) {
        const agent = this.agents[agentIndex];
        agent.alpha = 0.3; // Reset to default appearance
    }

    updateAgentInfo(agentIndex) {
        const agent = this.agents[agentIndex];
        document.getElementById("agentid").innerHTML = agentIndex.toString();
        document.getElementById("agentx").innerHTML = agent.x.toFixed(2);
        document.getElementById("agenty").innerHTML = agent.y.toFixed(2);
    }

    addTickerUpdates() {
        this.ticker = new PIXI.Ticker();
        this.ticker.add(() => this.updateDebugInfo());
        this.ticker.start();
    }

    updateDebugInfo() {
        document.getElementById("mousepos").innerHTML = `X=${this.mousePos.x.toFixed(2)} Y=${this.mousePos.y.toFixed(2)}`;
        document.getElementById("zoomscale").innerHTML = this.zoomScaleFactor.toFixed(2);
        document.getElementById("gridpos").innerHTML = `X=${this.grid.x.toFixed(2)} Y=${this.grid.y.toFixed(2)}`;
    }
}

document.addEventListener("DOMContentLoaded", function() {
    // get config from server
    const config = fetch('http://localhost:8080/config', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }}).then(response => response.json().then(data => {
            console.log(data);  
            const app = new Application(data.numAgents, data.height, data.width , data.cellSize, data.agentRadius);
            app.initialize();
        })
    );
});
