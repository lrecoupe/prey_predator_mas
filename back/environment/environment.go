package environment

import (
	"Prey_Predator_MAS/agents"
	"Prey_Predator_MAS/config"
	"Prey_Predator_MAS/fixedgrid"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

type Environment struct {
	Width, Height    int
	Agents           []*agents.Agent
	IterationDone    chan bool
	wg               sync.WaitGroup
	fixedGrid        fixedgrid.FixedGrid
	predatorPerceipt agents.Perceipt
	preyPerceipt     agents.Perceipt
}

func NewEnvironment(width, height, numAgents, fixedGridCellSize int) *Environment {
	config := config.GetDefaultConfig()
	env := &Environment{
		Width:            width,
		Height:           height,
		Agents:           make([]*agents.Agent, 0),
		IterationDone:    make(chan bool),
		fixedGrid:        fixedgrid.NewFixedGrid(),
		predatorPerceipt: agents.NewPredatorPerceipt(config.RayNumber, config.PredatorRayLength, float64(config.PredatorRayAngleDeg)),
		preyPerceipt:     agents.NewPreyPerceipt(config.RayNumber, config.PreyRayLength, float64(config.PreyRayAngleDeg)),
	}

	for i := 0; i < numAgents; i++ {
		// init agents
		var agentColor string
		var perceipt agents.Perceipt
		if i%2 == 0 {
			agentColor = "Red"
			perceipt = env.predatorPerceipt
		} else {
			agentColor = "Green"
			perceipt = env.preyPerceipt
		}
		var x, y float64
		x = float64(rand.Intn(width - 1))
		y = float64(rand.Intn(height - 1))

		env.Agents = append(env.Agents, agents.NewAgent(uint16(i), x, y, agentColor, perceipt))

		// add agent to fixed grid
		env.fixedGrid.AddAgent(env.Agents[i])
	}
	return env
}

func (e *Environment) Start() {
	for {
		start := time.Now()
		// Perception phase
		e.wg.Add(len(e.Agents))
		for _, agent := range e.Agents {
			go func(agent *agents.Agent, fixedGrid *fixedgrid.FixedGrid) {
				defer e.wg.Done()
				agent.Perceipt.Perceive(agent, fixedGrid)
			}(agent, &e.fixedGrid)
		}
		e.wg.Wait() // Wait for all agents to complete the perception phase

		// Action phase
		e.wg.Add(len(e.Agents))
		for _, agent := range e.Agents {
			go func(agent *agents.Agent) {
				defer e.wg.Done()
				oldPos := agent.Act()
				if agent.ChangedCell() {
					e.fixedGrid.RemoveAgent(agent, oldPos)
					e.fixedGrid.AddAgent(agent)
				}
			}(agent)
		}
		e.wg.Wait() // Wait for all agents to complete the action phase
		elapsed := time.Since(start)

		// limit at 60 updates per second
		time.Sleep(time.Second/60 - elapsed)
		fmt.Printf("Iteration took %dms\n", elapsed.Milliseconds())
		e.IterationDone <- true
	}
}

func (e *Environment) LongPollIterationEnd() {
	<-e.IterationDone
}
