package config


const WIDTH = 1024
const HEIGHT = 1024
const NUM_AGENTS = 1024
const CELL_SIZE = 32
const APPROXIMATE_MAX_AGENT_PER_CELL = 20//NUM_AGENTS / ((HEIGHT / CELL_SIZE) * (WIDTH / CELL_SIZE)) * 4
const AGENT_RADIUS = 3
const RAY_NUMBER = 24
const PREDATOR_RAY_LENGTH = 32
const PREY_RAY_LENGTH = 8
const PREDATOR_RAY_ANGLE_DEG = 30
const PREY_RAY_ANGLE_DEG = 250

type Config struct {
	Width	            int `json:"width"`
	Height 		        int `json:"height"`
	NumAgents           int `json:"numAgents"`
	CellSize            int `json:"cellSize"`
	AgentRadius         int `json:"agentRadius"`
	RayNumber           int `json:"rayNumber"`
	PredatorRayLength   int `json:"predatorRayLength"`
	PreyRayLength       int `json:"preyRayLength"`
	PredatorRayAngleDeg int `json:"predatorRayAngleDeg"`
	PreyRayAngleDeg     int `json:"preyRayAngleDeg"`
}

func GetDefaultConfig() Config {
	return Config{
		Width:               WIDTH,
		Height:              HEIGHT,
		NumAgents:           NUM_AGENTS,
		CellSize:            CELL_SIZE,
		AgentRadius:         AGENT_RADIUS,
		RayNumber:           RAY_NUMBER,
		PredatorRayLength:   PREDATOR_RAY_LENGTH,
		PreyRayLength:       PREY_RAY_LENGTH,
		PredatorRayAngleDeg: PREDATOR_RAY_ANGLE_DEG,
		PreyRayAngleDeg:     PREY_RAY_ANGLE_DEG,
	}
}
