package agents

import (
	"Prey_Predator_MAS/config"
	"math/rand"

	"github.com/quartercastle/vector"
)

const (
	MaxSpeed = 2.0
)

type Agent struct {
	ID          uint16        `json:"id"`
	Position    vector.Vector `json:"pos"`
	Color       string        `json:"color"`
	Velocity    vector.Vector `json:"-"`
	Perceipt    Perceipt	  `json:"-"`
	RaysValues  []float64	  `json:"-"`
	changedCell bool		  `json:"-"`
}

type AgentViewModel struct {
    ID          uint16            `json:"id"`
    Position    vector.Vector     `json:"pos"`
    Color       string            `json:"color"`
    Velocity    *vector.Vector    `json:"vel,omitempty"`
    RaysValues  *[]float64        `json:"raysValues,omitempty"`
}

func NewAgentViewModel(agent *Agent, isSelected bool) *AgentViewModel {
    vm := &AgentViewModel{
        ID:       agent.ID,
        Position: agent.Position,
        Color:    agent.Color,
    }

    if isSelected {
        vm.Velocity = &agent.Velocity
        vm.RaysValues = &agent.RaysValues
    }

    return vm
}

func GetGridCell(x, y float64) (uint32, uint32) {
	cellSize := config.GetDefaultConfig().CellSize
	return uint32(x / float64(cellSize)), uint32(y / float64(cellSize))
}

func NewAgent(ID uint16, x, y float64, color string, perceipt Perceipt) *Agent {
	return &Agent{
		ID:         ID,
		Position:   vector.Vector{x, y},
		Color:      color,
		Perceipt:   perceipt,
		RaysValues: make([]float64, config.GetDefaultConfig().RayNumber),
	}
}

func (a *Agent) move() (oldPosition vector.Vector) {
	speed := rand.Float64() * MaxSpeed
	// random angle between 0 and 360 degrees
	rotation := rand.Float64() * 2 * 3.141592653589793

	// Initialize velocity along a fixed direction (e.g., along the X-axis)
	velocity := vector.Vector{1, 0} // Velocity along X-axis

	// Rotate the velocity vector
	velocity = velocity.Rotate(rotation)

	// Scale the velocity by speed
	velocity = velocity.Unit()
	velocity = velocity.Scale(speed)

	oldPosition = a.Position
	a.Position = a.Position.Add(velocity)
	if !a.validatePosition() {
		a.Position = oldPosition
	} else {
		a.Velocity = velocity
	}

	a.checkCellChange(oldPosition)

	return oldPosition
}

func (a *Agent) validatePosition() bool {
	config := config.GetDefaultConfig()
	return a.Position.X() > 0 && a.Position.X() < float64(config.Width-1) &&
		a.Position.Y() > 0 && a.Position.Y() < float64(config.Width-1)
}

func (a *Agent) checkCellChange(oldPosition vector.Vector) {
	cellSize := float64(config.GetDefaultConfig().CellSize)
	if (a.Position.X()/cellSize != oldPosition.X()/cellSize) ||
		(a.Position.Y()/cellSize != oldPosition.Y()/cellSize) {
		a.changedCell = true
	}
}

func (a *Agent) Act() (oldPosition vector.Vector) {
	return a.move()
}

func (a *Agent) ChangedCell() bool {
	return a.changedCell
}
