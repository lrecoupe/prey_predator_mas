package main

import _ "net/http/pprof"
import "net/http"


import (
	"Prey_Predator_MAS/webserver"
)

func main() {
	go func() {
		http.ListenAndServe("localhost:6060", nil)
	}()
	server := webserver.NewWebServer("localhost", "8080")
	server.Start()
}
