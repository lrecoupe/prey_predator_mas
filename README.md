# Description du projet
- A compléter
# Prérequis
- Avoir installé Go
- Avoir installé Python
# Installation
- Cloner le projet dans un dossier
- Se placer dans le back, puis lancer la commande `go run .\main.go`
- Se placer dans le front, puis lancer la commande `python -m http.server 8000`
- Ouvrir un navigateur et se rendre à l'adresse `localhost:8000`

# Historiques des optimisations:


16 000 agents:
Front:
- AVG Framerate Base Version: 30ms
- With "disableFriendlyErrors": 18ms
- Passage sur pixiJS + utilisations de conteneurs + utilisation de webGL: 7ms = 144 FPS
